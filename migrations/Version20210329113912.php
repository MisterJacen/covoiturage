<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210329113912 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE adherent (id INT AUTO_INCREMENT NOT NULL, prenom VARCHAR(255) NOT NULL, nom VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE lieu (id INT AUTO_INCREMENT NOT NULL, latitude DOUBLE PRECISION NOT NULL, longitude DOUBLE PRECISION NOT NULL, nom VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE trajet (id INT AUTO_INCREMENT NOT NULL, auteur_id INT NOT NULL, lieu_depart_id INT NOT NULL, lieu_arrivee_id INT NOT NULL, date_depart DATETIME NOT NULL, nb_places INT NOT NULL, INDEX IDX_2B5BA98C60BB6FE6 (auteur_id), INDEX IDX_2B5BA98CC16565FC (lieu_depart_id), INDEX IDX_2B5BA98CBF9A3FF6 (lieu_arrivee_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE trajet_utilisateur (trajet_id INT NOT NULL, utilisateur_id INT NOT NULL, INDEX IDX_ED0C1620D12A823 (trajet_id), INDEX IDX_ED0C1620FB88E14F (utilisateur_id), PRIMARY KEY(trajet_id, utilisateur_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE utilisateur (id INT AUTO_INCREMENT NOT NULL, adherent_id INT DEFAULT NULL, email VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, pseudo VARCHAR(255) NOT NULL, INDEX IDX_1D1C63B325F06C53 (adherent_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE trajet ADD CONSTRAINT FK_2B5BA98C60BB6FE6 FOREIGN KEY (auteur_id) REFERENCES adherent (id)');
        $this->addSql('ALTER TABLE trajet ADD CONSTRAINT FK_2B5BA98CC16565FC FOREIGN KEY (lieu_depart_id) REFERENCES lieu (id)');
        $this->addSql('ALTER TABLE trajet ADD CONSTRAINT FK_2B5BA98CBF9A3FF6 FOREIGN KEY (lieu_arrivee_id) REFERENCES lieu (id)');
        $this->addSql('ALTER TABLE trajet_utilisateur ADD CONSTRAINT FK_ED0C1620D12A823 FOREIGN KEY (trajet_id) REFERENCES trajet (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE trajet_utilisateur ADD CONSTRAINT FK_ED0C1620FB88E14F FOREIGN KEY (utilisateur_id) REFERENCES utilisateur (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE utilisateur ADD CONSTRAINT FK_1D1C63B325F06C53 FOREIGN KEY (adherent_id) REFERENCES adherent (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE trajet DROP FOREIGN KEY FK_2B5BA98C60BB6FE6');
        $this->addSql('ALTER TABLE utilisateur DROP FOREIGN KEY FK_1D1C63B325F06C53');
        $this->addSql('ALTER TABLE trajet DROP FOREIGN KEY FK_2B5BA98CC16565FC');
        $this->addSql('ALTER TABLE trajet DROP FOREIGN KEY FK_2B5BA98CBF9A3FF6');
        $this->addSql('ALTER TABLE trajet_utilisateur DROP FOREIGN KEY FK_ED0C1620D12A823');
        $this->addSql('ALTER TABLE trajet_utilisateur DROP FOREIGN KEY FK_ED0C1620FB88E14F');
        $this->addSql('DROP TABLE adherent');
        $this->addSql('DROP TABLE lieu');
        $this->addSql('DROP TABLE trajet');
        $this->addSql('DROP TABLE trajet_utilisateur');
        $this->addSql('DROP TABLE utilisateur');
    }
}
