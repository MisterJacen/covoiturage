# Projet covoiturage

## Scénarios

* Bonne réponse renvoyé par les différentes pages
* Bon contenu renvoyé par la page d'accueil
* Bon contenu renvoyé par la page d'inscription
* Bonne instanciation des classes Adhérent, Lieu et Trajet et utilisateur
* Bonne vérification du format des e-mails pour les utilisateurs
* Bonne gestion du formulaire d'inscription
* Bon fonctionnement du reposotitory de Trajet avec un jeu de données fourni