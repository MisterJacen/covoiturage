<?php
namespace App\Controller;

use App\Entity\Trajet;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class DefaultController extends AbstractController
{
    public function index() {
        $trajets = $this->getDoctrine()->getRepository(Trajet::class)->getFutursTrajet();
        return $this->render('home.html.twig', [
            "trajets" => $trajets
        ]);
    }
}

?>