<?php
namespace App\Controller;

use App\Form\UtilisateurType;
use App\Entity\Utilisateur;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class UtilisateurController extends AbstractController
{
    public function inscription(Request $request) {
		// Création du formulaire
        $user = new Utilisateur();
        $form = $this->createForm(UtilisateurType::class, $user);

		// Gère les données du formulaire en cas de requête POST
        $form->handleRequest($request);
		// Si on a soumit le formulaire
        if ($form->isSubmitted() && $form->isValid()) {

			// Sauvegarde du nouvel utilisateur
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

			// On redirige à l'accueil
            return $this->redirectToRoute('index');
        }

        return $this->render(
            'inscription.html.twig',
            array('form' => $form->createView())
        );
    }
}

?>