<?php
namespace App\DataFixtures;

use App\Entity\Adherent;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AdherentFixtures extends Fixture
{
    public const ADHERENT_BASE = 'maxence';

    public function load(ObjectManager $manager)
    {
        $adherent = new Adherent();
        $adherent->setPrenom('Maxence');
        $adherent->setNom('Bonsergent');

        $manager->persist($adherent);
        $manager->flush();

        $this->addReference(self::ADHERENT_BASE, $adherent);
    }
}
