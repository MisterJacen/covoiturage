<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\Lieu;

class LieuFixtures extends Fixture
{

    public const LIEU_GRENOBLE = 'grenoble';
    public const LIEU_CHAMBERY = 'chambéry';

    public function load(ObjectManager $manager)
    {
        $grenoble = new Lieu();
        $grenoble->setNom('Grenoble');
        $grenoble->setLatitude(45.1840392);
        $grenoble->setLongitude(5.680523);
        $manager->persist($grenoble);

        $chambery = new Lieu();
        $chambery->setNom('Chambéry');
        $chambery->setLatitude(45.5822142);
        $chambery->setLongitude(5.871334);
        $manager->persist($chambery);

        $manager->flush();

        $this->addReference(self::LIEU_GRENOBLE, $grenoble);
        $this->addReference(self::LIEU_CHAMBERY, $chambery);
    }
}
