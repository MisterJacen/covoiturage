<?php

namespace App\DataFixtures;

use App\DataFixtures\LieuFixtures;
use App\Entity\Trajet;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class TrajetFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $grenoble = $this->getReference(LieuFixtures::LIEU_GRENOBLE);
        $chambery = $this->getReference(LieuFixtures::LIEU_CHAMBERY);
        $adherent = $this->getReference(AdherentFixtures::ADHERENT_BASE);

        $trajets = [];

        for ($i = 0; $i < 20; $i++) {
            $minutesToAdd = $i*2+5;
            $trajet = new Trajet();
            $trajet->setLieuDepart($grenoble);
            $trajet->setLieuArrivee($chambery);
            $date = new \DateTime('now');
            $date->add(new \DateInterval('PT'. $minutesToAdd .'M'));
            $trajet->setDateDepart($date);
            $trajet->setNbPlaces($i+1);
            $trajet->setAuteur($adherent);
            $manager->persist($trajet);
            array_push($trajets, $trajet);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            LieuFixtures::class,
            AdherentFixtures::class
        ];
    }
}
