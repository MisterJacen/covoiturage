<?php

namespace App\Entity;

use App\Repository\LieuRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=LieuRepository::class)
 */
class Lieu
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="float")
     */
    private $latitude;

    /**
     * @ORM\Column(type="float")
     */
    private $longitude;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\OneToMany(targetEntity=Trajet::class, mappedBy="lieuDepart")
     */
    private $trajets;

    /**
     * @ORM\OneToMany(targetEntity=Trajet::class, mappedBy="lieuArrivee")
     */
    private $trajetsArrivees;

    public function __construct()
    {
        $this->trajets = new ArrayCollection();
        $this->trajetsArrivees = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLatitude(): ?float
    {
        return $this->latitude;
    }

    public function setLatitude(float $latitude): self
    {
        $this->latitude = $latitude;

        return $this;
    }

    public function getLongitude(): ?float
    {
        return $this->longitude;
    }

    public function setLongitude(float $longitude): self
    {
        $this->longitude = $longitude;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * @return Collection|Trajet[]
     */
    public function getTrajets(): Collection
    {
        return $this->trajets;
    }

    public function addTrajet(Trajet $trajet): self
    {
        if (!$this->trajets->contains($trajet)) {
            $this->trajets[] = $trajet;
            $trajet->setLieuDepart($this);
        }

        return $this;
    }

    public function removeTrajet(Trajet $trajet): self
    {
        if ($this->trajets->removeElement($trajet)) {
            // set the owning side to null (unless already changed)
            if ($trajet->getLieuDepart() === $this) {
                $trajet->setLieuDepart(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Trajet[]
     */
    public function getTrajetsArrivees(): Collection
    {
        return $this->trajetsArrivees;
    }

    public function addTrajetsArrivee(Trajet $trajetsArrivee): self
    {
        if (!$this->trajetsArrivees->contains($trajetsArrivee)) {
            $this->trajetsArrivees[] = $trajetsArrivee;
            $trajetsArrivee->setLieuArrivee($this);
        }

        return $this;
    }

    public function removeTrajetsArrivee(Trajet $trajetsArrivee): self
    {
        if ($this->trajetsArrivees->removeElement($trajetsArrivee)) {
            // set the owning side to null (unless already changed)
            if ($trajetsArrivee->getLieuArrivee() === $this) {
                $trajetsArrivee->setLieuArrivee(null);
            }
        }

        return $this;
    }
}
