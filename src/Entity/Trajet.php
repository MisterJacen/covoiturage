<?php

namespace App\Entity;

use App\Repository\TrajetRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TrajetRepository::class)
 */
class Trajet
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Adherent::class, inversedBy="trajets")
     * @ORM\JoinColumn(nullable=false)
     */
    private $auteur;

    /**
     * @ORM\Column(type="datetime")
     */
    private $DateDepart;

    /**
     * @ORM\Column(type="integer")
     */
    private $nbPlaces;

    /**
     * @ORM\ManyToMany(targetEntity=Utilisateur::class, inversedBy="trajets")
     */
    private $passagers;

    /**
     * @ORM\ManyToOne(targetEntity=Lieu::class, inversedBy="trajets")
     * @ORM\JoinColumn(nullable=false)
     */
    private $lieuDepart;

    /**
     * @ORM\ManyToOne(targetEntity=Lieu::class, inversedBy="trajetsArrivees")
     * @ORM\JoinColumn(nullable=false)
     */
    private $lieuArrivee;

    public function __construct()
    {
        $this->passagers = new ArrayCollection();
        $this->LieuDepart = new ArrayCollection();
        $this->LieuArrivee = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAuteur(): ?Adherent
    {
        return $this->auteur;
    }

    public function setAuteur(?Adherent $auteur): self
    {
        $this->auteur = $auteur;

        return $this;
    }

    public function getDateDepart(): ?\DateTimeInterface
    {
        return $this->DateDepart;
    }

    public function setDateDepart(\DateTimeInterface $DateDepart): self
    {
        $this->DateDepart = $DateDepart;

        return $this;
    }

    public function getNbPlaces(): ?int
    {
        return $this->nbPlaces;
    }

    public function setNbPlaces(int $nbPlaces): self
    {
        $this->nbPlaces = $nbPlaces;

        return $this;
    }

    /**
     * @return Collection|Utilisateur[]
     */
    public function getPassagers(): Collection
    {
        return $this->passagers;
    }

    public function addPassager(Utilisateur $passager): self
    {
        if (!$this->passagers->contains($passager)) {
            $this->passagers[] = $passager;
        }

        return $this;
    }

    public function removePassager(Utilisateur $passager): self
    {
        $this->passagers->removeElement($passager);

        return $this;
    }

    public function getLieuDepart(): ?Lieu
    {
        return $this->lieuDepart;
    }

    public function setLieuDepart(?Lieu $lieuDepart): self
    {
        $this->lieuDepart = $lieuDepart;

        return $this;
    }

    public function getLieuArrivee(): ?Lieu
    {
        return $this->lieuArrivee;
    }

    public function setLieuArrivee(?Lieu $lieuArrivee): self
    {
        $this->lieuArrivee = $lieuArrivee;

        return $this;
    }
}
