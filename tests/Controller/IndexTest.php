<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class IndexTest extends WebTestCase {

    /**
     * Vérifie que le contenu retourné par le controller est le bon.
     */
    public function testRenderGoodContent()
    {
        $client = self::createClient();
        $client->request('GET', '/');

		$this->assertSelectorTextContains('html body h1', 'Accueil - Site de covoiturage');
    }
}