<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class InscriptionTest extends WebTestCase {

    /**
     * Vérifie que le contenu retourné par le controller est le bon.
     */
    public function testRenderGoodContent()
    {
        $client = self::createClient();
        $client->request('GET', '/inscription');

		$this->assertSelectorTextContains('html body h1', 'Inscription - Site de covoiturage');
    }
}