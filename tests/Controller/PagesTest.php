<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class PagesTest extends WebTestCase {
    /**
     * @dataProvider provideUrls
     */
    public function testPageIsSuccessful($url)
    {
        $client = self::createClient();
        $client->request('GET', $url);
    
        // Vérifie que la page renvoie une bonne réponse
        $this->assertTrue($client->getResponse()->isSuccessful());
    }
    
    public function provideUrls()
    {
        return [
            ['/'],
        ];
    }
}