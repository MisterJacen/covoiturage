<?php

namespace App\Tests\Entity;

use PHPUnit\Framework\TestCase;
use App\Entity\Utilisateur;
use UnexpectedValueException;

require_once 'src/Entity/Utilisateur.php';

class UtilisateurTest extends TestCase
{
    public function testNewUtilisateur(): void
    {
        $utilisateur = new Utilisateur();        
        $this->assertInstanceOf(Utilisateur::class, $utilisateur);
    }

    
    public function emailProvider()
    {
        return [
            ["test", true],
            ["test@test", true],
            ["test@test.c", false],
            ["test@test.com", false],
        ];
    }

    /**
     * @dataProvider emailProvider
     */
    public function testEmailUtilisateur($email, $shouldRaiseException): void
    {
        $utilisateur = new Utilisateur();
        if ($shouldRaiseException) {
            $this->expectException(UnexpectedValueException::class);
        }
        $utilisateur->setEmail($email);
        $this->assertEquals($email, $utilisateur->getEmail());
    }
}
