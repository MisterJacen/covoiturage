<?php
namespace App\Tests\Form\Type;

use App\Entity\Utilisateur;
use App\Form\UtilisateurType;
use Symfony\Component\Form\Test\TypeTestCase;

class UtilisateurTypeTest extends TypeTestCase
{
    public function testSubmitValidData()
    {
		// Les données du formulaire
        $formData = [
            'email' => 'test@test.com',
            'password' => array('first' => 'pass', 'second' => 'pass'),
            'pseudo' => 'JeanMichel',
        ];

        $model = new Utilisateur();
        // Création du formulaire de Type UtilisateurType avec des données Utilisateurs
        // (vide pour l'instant, il sera rempli par le submit)
        $form = $this->factory->create(UtilisateurType::class, $model);

        // On créer l'objet qui va nous servir de comparaison
        $expected = new Utilisateur();
        $expected->setEmail($formData['email']);
        $expected->setPassword($formData['password']['first']);
        $expected->setPseudo($formData['pseudo']);

        // On soumet les données au formulaire
        $form->submit($formData);

        // On vérifie qu'il n'y a pas d'erreur dans la transformation
        $this->assertTrue($form->isSynchronized());

        // On vérifie qu'on récupère bien le résultat attendu
        $this->assertEquals($expected, $model);
    }
}