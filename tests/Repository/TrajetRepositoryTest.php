<?php
namespace App\Tests\Repository;

use App\DataFixtures\TrajetFixtures;
use App\Entity\Trajet;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Loader;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ProductRepositoryTest extends KernelTestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $loader = new Loader();
        $loader->addFixture(new TrajetFixtures());

        $purger = new ORMPurger($this->entityManager);
        $executor = new ORMExecutor($this->entityManager, $purger);
        $executor->execute($loader->getFixtures());
    }

    public function testFutursTrajets()
    {
        $trajets = $this->entityManager
            ->getRepository(Trajet::class)
			->getFutursTrajet();

        $this->assertSame(5, count($trajets));
        // Vérification que l'ordre des trajets est bon
        // Dans le fixtures, le nombre de place correspond à leur index
        $this->assertEquals(1, $trajets[0]->getNbPlaces());
        $this->assertEquals(2, $trajets[1]->getNbPlaces());
        $this->assertEquals(3, $trajets[2]->getNbPlaces());
        $this->assertEquals(4, $trajets[3]->getNbPlaces());
        $this->assertEquals(5, $trajets[4]->getNbPlaces());
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        // doing this is recommended to avoid memory leaks
        $this->entityManager->close();
        $this->entityManager = null;
    }
}